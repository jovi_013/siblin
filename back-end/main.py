import email
from fastapi import FastAPI, Form, Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from init_db import init_node
from pydantic import BaseModel
from time import time
from hashlib import sha1
from datetime import datetime

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

user_db = {}
node1, node2, node3, node4, node5 = init_node()

user_db[node1.email] = node1
user_db[node2.email] = node2
user_db[node3.email] = node3
user_db[node4.email] = node4
user_db[node5.email] = node5

class Credentials(BaseModel):
    email: str
    password: str

def create_token(email):
    byte_str = bytes((email + str(time())), "utf-8")
    token = sha1(byte_str).hexdigest()
    return token

@app.get("/")
def read_root():
    return "Please see url /docs for documentation."

@app.post("/login")
def login(credentials: Credentials):
    error_msg = jsonable_encoder({"error": "401 Unauthorized"})

    try:
        _user = user_db[credentials.email] # if user not exist, throw exception

        # check credentials
        if (credentials.password != _user.password):
            return JSONResponse(status_code=status.HTTP_401_UNAUTHORIZED, content=error_msg)
    except:
        return JSONResponse(status_code=status.HTTP_401_UNAUTHORIZED, content=error_msg)

    response = jsonable_encoder({"token": create_token(credentials.email)})

    return JSONResponse(content=response)
from pydantic import BaseModel

class UserAccount(BaseModel):
    email: str
    password: str
    pubkey: str

owner = UserAccount(
    email="node1@example.com",
    password="node1",
    pubkey="0x15d9Dff6715E14aFD2E5C80cc2523259555D93F2"
)

node2 = UserAccount(
    email="node2@example.com",
    password="node2",
    pubkey="0xf128785215cEe6AA41ED5843b6fb8a849d835351"
)

node3 = UserAccount(
    email="node3@example.com",
    password="node3",
    pubkey="0x962c7DCa4CD9A5b2153417ee78fC1045E163e970"
)

node4 = UserAccount(
    email="node4@example.com",
    password="node4",
    pubkey="0xF5504BC1cf7e53bdf40F80E4d0B890bE8672a8Dc"
)

node5 = UserAccount(
    email="node5@example.com",
    password="node5",
    pubkey="0x500F437fc6BAd6c71240F8bCB0689B29d9b95f63"
)

def init_node():
    return owner, node2, node3, node3, node5

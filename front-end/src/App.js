import { useEffect, useState } from 'react';
import Web3 from 'web3';
import './App.css';
import PageWrapper from './components/PageWrapper';
import { createUseStyles } from "react-jss";
import { MY_CONTRACT_ABI, MY_CONTRACT_ADDRESS } from './config';
import { Button, Heading, Pane, Text, TextInputField } from 'evergreen-ui';
import { useNavigate } from 'react-router-dom';
import useToken from './hook/useToken';
import { addCourier, addIssuer, createBL, extractAccountData, getBillOfLading, getCourier, getIssuer, issuerSign, renderTableBasicTxReceipt, renderTableBillOfLadingCheck, renderTableBillOfLadingReceipt, renderTableCourierReceipt, renderTableIssuerReceipt, verifySignature } from './helper';
import { ethers } from 'ethers';

const useStyles = createUseStyles({
  lineBreak: {
    marginTop: "30px",
    marginBottom: "30px",
    borderColor: "#85A3FF",
  }
})

function App() {
  const [account, setAccount] = useState();
  const [contract, setContract] = useState();
  const [web3Connection, setWeb3Connection] = useState();

  // Add Issuer state
  const [issuerNameValue, setIssuerNameValue] = useState('');
  const [issuerAddressValue, setIssuerAddressValue] = useState('');
  const [response1, setResponse1] = useState('');
  const [data1, setData1] = useState('');

  // Add Courier state
  const [courierNameValue, setCourierNameValue] = useState('');
  const [courierAddressValue, setCourierAddressValue] = useState('');
  const [response2, setResponse2] = useState('');
  const [data2, setData2] = useState('');

  // Create Bill of Lading state
  const [BLGoodsValue, setBLGoodsValue] = useState('');

  const [shipperName, setShipperName] = useState('');
  const [shipperHomeAddress, setShipperHomeAddress] = useState('');
  const [shipperPhoneNumber, setShipperPhoneNumber] = useState('');
  const [shipperPubKey, setShipperPubKey] = useState('');

  const [receiverName, setReceiverName] = useState('');
  const [receiverHomeAddress, setReceiverHomeAddress] = useState('');
  const [receiverPhoneNumber, setReceiverPhoneNumber] = useState('');
  const [receiverPubKey, setReceiverPubKey] = useState('');  
  const [response3, setResponse3] = useState('');

  // Check Bill of Lading state
  const [BLNumber, setBLNumber] = useState('');
  const [data3, setData3] = useState('');

  // Sign state
  const [dataToBeSigned, setDataToBeSigned] = useState('');
  const [response4, setResponse4] = useState('');

  // Verify state
  const [signerAddress, setSignerAddress] = useState('');
  const [rawData, setRawData] = useState('');
  const [signerSignature, setSignerSignature] = useState('');
  const [response5, setResponse5] = useState(null);

  // Send Ether state
  const [recipientAddress, setRecipientAddress] = useState('');
  const [etherAmount, setEtherAmount] = useState('');
  const [response6, setResponse6] = useState('');

  // Courier self-assign state
  const [BLNumber2, setBLNumber2] = useState('');
  const [response7, setResponse7] = useState('');

  const navigate = useNavigate();
  const classes = useStyles();

  const { token, setToken } = useToken();

  useEffect(() => {
    if (!token) {
      navigate("/login")
    }
  }, [])

  useEffect(() => {
    const getDataWrapper = async() => {
      const web3 = new Web3(process.env.REACT_APP_NODE1);
      web3.eth.getAccounts().then((accounts) => setAccount(accounts[0]));

      const myContract = new web3.eth.Contract(MY_CONTRACT_ABI, MY_CONTRACT_ADDRESS);
      setContract(myContract);
      setWeb3Connection(web3)
    }
    getDataWrapper();
  }, [])

  return (
    <PageWrapper>
      <Pane>
        <Heading>Add Issuer (owner only)</Heading>
        <TextInputField
          label="Name"
          description="Full name"
          placeholder="Lorem ipsum"
          value={issuerNameValue}
          onChange={e => setIssuerNameValue(e.target.value)}
        />

        <TextInputField
          label="Address"
          description="Ethereum address / public key"
          placeholder="0x0000000000000000000000000000000000000000"
          value={issuerAddressValue}
          onChange={e => setIssuerAddressValue(e.target.value)}
        />

        <Button
          onClick={async () => {
            let _response = await addIssuer(contract, issuerNameValue, issuerAddressValue, account);
            let _data = await getIssuer(contract, issuerAddressValue)
            setResponse1(_response);
            setData1(_data);
            setIssuerNameValue('');
            setIssuerAddressValue('');
          }}
          width="100%"
        >
          --- Add Issuer ---
        </Button>

        {response1 && renderTableIssuerReceipt(response1, data1)}

        <hr className={classes.lineBreak}/>

        <Heading>Add Courier (issuer only)</Heading>
        <TextInputField
          label="Name"
          description="Full name"
          placeholder="Lorem ipsum"
          value={courierNameValue}
          onChange={e => setCourierNameValue(e.target.value)}
        />

        <TextInputField
          label="Address"
          description="Ethereum address / public key"
          placeholder="0x0000000000000000000000000000000000000000"
          value={courierAddressValue}
          onChange={e => setCourierAddressValue(e.target.value)}
        />

        <Button
          onClick={async () => {
            let _response = await addCourier(contract, courierNameValue, courierAddressValue, account)
            let _data = await getCourier(contract, courierAddressValue)
            setResponse2(_response);
            setData2(_data);
            setCourierNameValue('');
            setCourierAddressValue('');
          }}
          width="100%"
        >
          --- Add Courier ---
        </Button>

        {response2 && renderTableCourierReceipt(response2, data2)}

        <hr className={classes.lineBreak}/>

        <Heading>Create Bill of Lading (issuer only)</Heading>
        
        <TextInputField
          label="Date"
          description="Today's date"
          value={(new Date).toString().slice(0,15)}
          disabled
        />

        <TextInputField
          label="Goods"
          placeholder="Table 10, Chair 10, Bed 10"
          value={BLGoodsValue}
          onChange={e => setBLGoodsValue(e.target.value)}
        />

        <TextInputField
          label="Shipper's Name"
          placeholder="Alice"
          value={shipperName}
          onChange={e => setShipperName(e.target.value)}
        />

        <TextInputField
          label="Shipper's Address"
          description="Location"
          placeholder="Jl. XYZ no.123, Jakarta, Indonesia"
          value={shipperHomeAddress}
          onChange={e => setShipperHomeAddress(e.target.value)}
        />

        <TextInputField
          label="Shipper's Phone Number"
          description="Mobile phone number"
          placeholder="(+62) 81234567890"
          value={shipperPhoneNumber}
          onChange={e => setShipperPhoneNumber(e.target.value)}
        />

        <TextInputField
          label="Shipper's Public Key"
          description="Ethereum address"
          placeholder="0x031705d75fc32e94e815f62fb9AfD524C7a1B20a"
          value={shipperPubKey}
          onChange={e => setShipperPubKey(e.target.value)}
        />

        <TextInputField
          label="Receiver's Name"
          placeholder="Bob"
          value={receiverName}
          onChange={e => setReceiverName(e.target.value)}
        />

        <TextInputField
          label="Receiver's Address"
          description="Location"
          placeholder="932 Turkey Pen Lane, Long Beach, California"
          value={receiverHomeAddress}
          onChange={e => setReceiverHomeAddress(e.target.value)}
        />

        <TextInputField
          label="Receiver's Phone Number"
          description="Mobile phone number"
          placeholder="(+1) 334-444-8132"
          value={receiverPhoneNumber}
          onChange={e => setReceiverPhoneNumber(e.target.value)}
        />

        <TextInputField
          label="Receiver's Public Key"
          description="Ethereum address"
          placeholder="0x0aD9Ff1f5DE716ae675C2dA1940a307Dc0892374"
          value={receiverPubKey}
          onChange={e => setReceiverPubKey(e.target.value)}
        />

        <Button
          onClick={async () => {
            let today = (new Date).toString().slice(0,15)
            let _shipperData = [shipperName, shipperHomeAddress, shipperPhoneNumber, shipperPubKey]
            let _receiverData = [receiverName, receiverHomeAddress, receiverPhoneNumber, receiverPubKey]

            let _response = await createBL(contract, today, BLGoodsValue, _shipperData, _receiverData, account)
            setResponse3(_response)
          }}
          width="100%"
        >
          --- Create Bill of Lading ---
        </Button>

        {response3 && renderTableBillOfLadingReceipt(response3)}

        <hr className={classes.lineBreak}/>
        
        <Heading>Check Bill of Lading</Heading>
        <TextInputField
          label="Number"
          description="Your Bill of Lading number"
          placeholder="101"
          value={BLNumber}
          onChange={e => setBLNumber(e.target.value)}
        />

        <Button
          onClick={async () => {
            let _data = await getBillOfLading(contract, BLNumber)
            setData3(_data)

            let _rawData = (
              _data.dateOfIssue + 
              JSON.stringify(data3.issuer) + 
              JSON.stringify(data3.shipper) + 
              JSON.stringify(data3.receiver) + 
              _data.goods
            )
            setRawData(_rawData)
            
            let _dataToBeSigned = await web3Connection.utils.sha3(_rawData)
            setDataToBeSigned(_dataToBeSigned)
          }}
          width="100%"
        >
          --- Check Bill of Lading ---
        </Button>

        {data3 && renderTableBillOfLadingCheck(data3)}

        <Heading marginY="10px">--- Sign ---</Heading>

        <Pane display="flex">
          <Button
            onClick={async () => {
              let signature = await web3Connection.eth.personal.sign(dataToBeSigned, account, process.env.REACT_APP_NODE1_PASSWORD)
              let today = (new Date).toString().slice(0,15)
              let _response = await issuerSign(contract, BLNumber, signature, today, account)
              setResponse4(_response)
            }}
            width="100%"
          >
            --- Issuer Sign ---
          </Button>

          <Button
            onClick={async () => {
              await window.ethereum.send("eth_requestAccounts")
              const provider = new ethers.providers.Web3Provider(window.ethereum)
              const signer = provider.getSigner();
              const signature = await signer.signMessage(dataToBeSigned)
              let today = (new Date).toString().slice(0,15)
              let _account = JSON.parse(sessionStorage.getItem('token'))?.token

              const transactionParameters = {
                to: MY_CONTRACT_ADDRESS,
                from: _account,
                data: contract.methods.shipperSign(BLNumber, signature, today).encodeABI(),
              };

              let txHash = await window.ethereum.request({
                method: "eth_sendTransaction",
                params: [transactionParameters],
              });
              
              let receipt = await (await provider.getTransaction(txHash).then((receipt) => { return receipt })).wait()
              setResponse4(receipt)
            }}
            width="100%"
          >
            --- Shipper Sign ---
          </Button>

          <Button
            onClick={async () => {
              await window.ethereum.send("eth_requestAccounts")
              const provider = new ethers.providers.Web3Provider(window.ethereum)
              const signer = provider.getSigner();
              const signature = await signer.signMessage(dataToBeSigned)
              let today = (new Date).toString().slice(0,15)
              let _account = JSON.parse(sessionStorage.getItem('token'))?.token

              const transactionParameters = {
                to: MY_CONTRACT_ADDRESS,
                from: _account,
                data: contract.methods.receiverSign(BLNumber, signature, today).encodeABI(),
              };

              let txHash = await window.ethereum.request({
                method: "eth_sendTransaction",
                params: [transactionParameters],
              });
              
              let receipt = await (await provider.getTransaction(txHash).then((receipt) => { return receipt })).wait()
              setResponse4(receipt)
            }}
            width="100%"
          >
            --- Receiver Sign ---
          </Button>

          <Button
            onClick={async () => {
              await window.ethereum.send("eth_requestAccounts")
              const provider = new ethers.providers.Web3Provider(window.ethereum)
              const signer = provider.getSigner();
              const signature = await signer.signMessage(dataToBeSigned)
              let today = (new Date).toString().slice(0,15)
              let _account = JSON.parse(sessionStorage.getItem('token'))?.token

              const transactionParameters = {
                to: MY_CONTRACT_ADDRESS,
                from: _account,
                data: contract.methods.courierSign(BLNumber, signature, today).encodeABI(),
              };

              let txHash = await window.ethereum.request({
                method: "eth_sendTransaction",
                params: [transactionParameters],
              });
              
              let receipt = await (await provider.getTransaction(txHash).then((receipt) => { return receipt })).wait()
              setResponse4(receipt)
            }}
            width="100%"
          >
            --- Courier Sign ---
          </Button>
        </Pane>

        {response4 && renderTableBasicTxReceipt(response4)}

        <Heading marginY="10px">--- Verify <span style={{color: "red"}}>(retrieve the Bill of Lading first)</span> ---</Heading>
        <TextInputField
          label="Signer Address"
          description="Enter signer address / public key"
          placeholder="0x0aD9Ff1f5DE716ae675C2dA1940a307Dc0892374"
          value={signerAddress}
          onChange={e => setSignerAddress(e.target.value)}
        />

        <TextInputField
          label="Signer Signature"
          description="Enter signer signature"
          placeholder="0x02cbda0294c60cd7f094598d95c8ba484c60d1f2e0ec472c9bfa8ecfe248a3206e01a5f8b1f6ebd7fa16f8e3b8b495ba753ed1bb5aa5a6919c9f9143d11230351c"
          value={signerSignature}
          onChange={e => setSignerSignature(e.target.value)}
        />
        <Pane>
          <Button
            onClick={async () => {
              let _response = await verifySignature(contract, signerAddress, rawData, signerSignature)
              setResponse5(_response)
            }}
            width="25%"
          >
            --- Verify Issuer ---
          </Button>

          <Button
            onClick={async () => {
              const _recoveredAddress = await ethers.utils.verifyMessage(dataToBeSigned, signerSignature)
              let _response = _recoveredAddress == signerAddress
              setResponse5(_response)
            }}
            width="75%"
          >
            --- Verify Shipper / Receiver / Courier ---
          </Button>
        </Pane>

        {response5 == true && <Text color="green">True, the signature is valid / was created by the address</Text>}
        {response5 == false && <Text color="red">False, the signature is invalid / was not created by the address</Text>}

        <hr className={classes.lineBreak}/>
        
        <Heading marginY="10px">Deliver goods (courier self-assign)</Heading>

        <TextInputField
          label="Enter Bill of Lading number"
          placeholder="101"
          value={BLNumber2}
          onChange={e => setBLNumber2(e.target.value)}
        />

        <Button
          onClick={async () => {
            await window.ethereum.send("eth_requestAccounts")
            const provider = new ethers.providers.Web3Provider(window.ethereum)
            let _account = JSON.parse(sessionStorage.getItem('token'))?.token

            const transactionParameters = {
              to: MY_CONTRACT_ADDRESS,
              from: _account,
              data: contract.methods.selfAssignCourier(BLNumber2).encodeABI(),
            };

            let txHash = await window.ethereum.request({
              method: "eth_sendTransaction",
              params: [transactionParameters],
            });
            
            let receipt = await (await provider.getTransaction(txHash).then((receipt) => { return receipt })).wait()
            setResponse7(receipt)
          }}
          width="100%"
        >
          --- Assign ---
        </Button>

        {response7 && renderTableBasicTxReceipt(response7)}

        <hr className={classes.lineBreak}/>

        <Heading marginY="10px">Send ETH to another user (for transaction fee)</Heading>

        <TextInputField
          label="Recipient address"
          description="Recipient Ethereum public key"
          placeholder="0x0aD9Ff1f5DE716ae675C2dA1940a307Dc0892374"
          value={recipientAddress}
          onChange={e => setRecipientAddress(e.target.value)}
        />

        <TextInputField
          label="ETH amount"
          placeholder="0.02"
          value={etherAmount}
          onChange={e => setEtherAmount(e.target.value)}
        />

        <Button
          onClick={async () => {
            const amountToSend = await web3Connection.utils.toWei(etherAmount, "ether"); // Convert to wei value
            let _response = await web3Connection.eth.sendTransaction({ from: account, to: recipientAddress, value: amountToSend });
            setResponse6(_response)
          }}
          width="100%"
        >
          --- Send ethers ---
        </Button>

        {response6 && renderTableBasicTxReceipt(response6)}

        <hr className={classes.lineBreak}/>

        <Pane height="500px"/>
      </Pane>
    </PageWrapper>
  );
}

export default App;

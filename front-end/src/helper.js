import { Heading, Pane, Table } from 'evergreen-ui';

export async function addIssuer(contract, name, address, senderAccount) {
  return await contract.methods.addIssuer(name, address)
    .send({from: senderAccount})
    .then((receipt) => { return receipt })
}

export async function getIssuer(contract, issuerAddress) {
  return await contract.methods.issuerStorage(issuerAddress)
    .call()
    .then((result) => { return result });
}

export async function addCourier(contract, name, address, senderAccount) {
  return await contract.methods.addCourier(name, address)
    .send({from: senderAccount})
    .then((receipt) => { return receipt });
}

export async function getCourier(contract, courierAddress) {
  return await contract.methods.courierStorage(courierAddress)
    .call()
    .then((result) => { return result });
}

export async function getBillOfLading(contract, BLNumber) {
  return await contract.methods.blStorage(BLNumber)
    .call()
    .then((result) => { return result });
}

export async function issuerSign(contract, BLNumber, signatureHash, date, senderAccount) {
  return await contract.methods.issuerSign(BLNumber, signatureHash, date) 
    .send({from: senderAccount})
    .then((receipt) => { return receipt });
}

export async function verifySignature(contract, signer, data, signature) {
  return await contract.methods.recoverAddress(signer, data, signature)
    .call()
    .then((result) => { return result })
}

export function extractAccountData(account) {
  return (account.name + account.homeAddress + account.phoneNumber + account.pubkey)
}

export async function createBL(
  contract,
  date,
  goods,
  shipperData,
  receiverData,
  senderAccount
) {
  return await contract.methods.createBL(date, goods, shipperData, receiverData)
    .send({from: senderAccount})
    .then((receipt) => { console.log(receipt); return receipt })
}

function tableTxReceiptHelper(_data) {
  return (
    <>
      <Table.Row>
        <Table.TextCell>blockHash</Table.TextCell>
        <Table.TextCell>{_data.blockHash}</Table.TextCell>
      </Table.Row>

      <Table.Row>
        <Table.TextCell>blockNumber</Table.TextCell>
        <Table.TextCell>{_data.blockNumber}</Table.TextCell>
      </Table.Row>

      <Table.Row>
        <Table.TextCell>from</Table.TextCell>
        <Table.TextCell>{_data.from}</Table.TextCell>
      </Table.Row>

      <Table.Row>
        <Table.TextCell>to</Table.TextCell>
        <Table.TextCell>{_data.to}</Table.TextCell>
      </Table.Row>

      <Table.Row>
        <Table.TextCell>transactionHash</Table.TextCell>
        <Table.TextCell>{_data.transactionHash}</Table.TextCell>
      </Table.Row>

      <Table.Row>
        <Table.TextCell>transactionIndex</Table.TextCell>
        <Table.TextCell>{_data.transactionIndex}</Table.TextCell>
      </Table.Row>
    </>
  )
}

export function renderTableBasicTxReceipt(_data) {
  return (
    <Pane>
      <Heading marginTop="30px">Receipt</Heading>
      <Table border>
        <Table.Body>
          {tableTxReceiptHelper(_data)}
        </Table.Body>
      </Table>
    </Pane>
  )
}

export function renderTableIssuerReceipt(response1, data1) {
  return (
    <Pane>
      <Heading marginTop="30px">Receipt</Heading>
      <Table border>
        <Table.Body>
          {tableTxReceiptHelper(response1)}

          <Table.Row>
            <Table.TextCell>REGISTERED ISSUER NAME</Table.TextCell>
            <Table.TextCell>{data1.name}</Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>REGISTERED ISSUER PUBLIC KEY</Table.TextCell>
            <Table.TextCell>{data1.pubkey}</Table.TextCell>
          </Table.Row>
        </Table.Body>
      </Table>
    </Pane>
  )
}

export function renderTableCourierReceipt(response2, data2) {
  return (
    <Pane>
      <Heading marginTop="30px">Receipt</Heading>
      <Table border>
        <Table.Body>
          {tableTxReceiptHelper(response2)}

          <Table.Row>
            <Table.TextCell>REGISTERED COURIER NAME</Table.TextCell>
            <Table.TextCell>{data2.name}</Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>REGISTERED COURIER PUBLIC KEY</Table.TextCell>
            <Table.TextCell>{data2.pubkey}</Table.TextCell>
          </Table.Row>
        </Table.Body>
      </Table>
    </Pane>
  )
}

export function renderTableBillOfLadingReceipt(response3) {
  return (
    <Pane>
      <Heading marginTop="30px">Receipt</Heading>
      <Table border>
        <Table.Body>
          {tableTxReceiptHelper(response3)}

          <Table.Row>
            <Table.TextCell>YOUR BILL OF LADING NUMBER (please note this)</Table.TextCell>
            <Table.TextCell>{response3.events.BillOfLadingCreation.returnValues.number}</Table.TextCell>
          </Table.Row>
        </Table.Body>
      </Table>
    </Pane>
  )
}

export function renderTableBillOfLadingCheck(data3) {
  return (
    <Pane>
      <Heading marginTop="30px">Bill of Lading information</Heading>
      <Table border>
        <Table.Body>
          <Table.Row>
            <Table.TextCell>Date of Issue</Table.TextCell>
            <Table.TextCell>{data3.dateOfIssue}</Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Issuer</Table.TextCell>
            <Table.TextCell>{data3.issuer.name}</Table.TextCell>
            <Table.TextCell>{data3.issuer.phoneNumber}</Table.TextCell>
            <Table.TextCell>{data3.issuer.homeAddress}</Table.TextCell>
            <Table.TextCell>{data3.issuer.pubkey}</Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Shipper</Table.TextCell>
            <Table.TextCell>{data3.shipper.name}</Table.TextCell>
            <Table.TextCell>{data3.shipper.phoneNumber}</Table.TextCell>
            <Table.TextCell>{data3.shipper.homeAddress}</Table.TextCell>
            <Table.TextCell>{data3.shipper.pubkey}</Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Receiver</Table.TextCell>
            <Table.TextCell>{data3.receiver.name}</Table.TextCell>
            <Table.TextCell>{data3.receiver.phoneNumber}</Table.TextCell>
            <Table.TextCell>{data3.receiver.homeAddress}</Table.TextCell>
            <Table.TextCell>{data3.receiver.pubkey}</Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Courier</Table.TextCell>
            <Table.TextCell>{data3.courier.name}</Table.TextCell>
            <Table.TextCell>{data3.courier.phoneNumber}</Table.TextCell>
            <Table.TextCell>{data3.courier.homeAddress}</Table.TextCell>
            <Table.TextCell>{data3.courier.pubkey}</Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Goods</Table.TextCell>
            <Table.TextCell>{data3.goods}</Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Issuer Signature |
              <span style={{color: "red"}}> Signing date: {data3.issuerSignature.signingDate} </span>
              | Signature: <span style={{color: "green"}}>{data3.issuerSignature.hash}</span>
            </Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Shipper Signature |
              <span style={{color: "red"}}> Signing date: {data3.shipperSignature.signingDate} </span>
              | Signature: <span style={{color: "green"}}>{data3.shipperSignature.hash}</span>
            </Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Receiver Signature |
              <span style={{color: "red"}}> Signing date: {data3.receiverSignature.signingDate} </span>
              | Signature: <span style={{color: "green"}}>{data3.receiverSignature.hash}</span>
            </Table.TextCell>
          </Table.Row>

          <Table.Row>
            <Table.TextCell>Courier Signature |
              <span style={{color: "red"}}> Signing date: {data3.courierSignature.signingDate} </span>
              | Signature: <span style={{color: "green"}}>{data3.courierSignature.hash}</span>
            </Table.TextCell>
          </Table.Row>
        </Table.Body>
      </Table>
    </Pane>
  )
}
import { Button, Pane, Text } from "evergreen-ui";
import { createUseStyles } from "react-jss";
import { Link, useNavigate } from "react-router-dom";

const useStyles = createUseStyles({
  navbarContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "10px"
  },
  contentContainer: {
    padding: "1rem 1rem",
    
  },
  "@media (min-width: 768px)": {
    contentContainer: {
      padding: "1rem 6rem",
    },
  },
})

export default function PageWrapper(props) {
  const classes = useStyles();

  return (
    <Pane>
      <Pane className={classes.navbarContainer} background="blue300">
        <Pane background="white" padding="5px" borderRadius="5px">
          <Text is={Link} to="/" fontWeight={700}>Home</Text>
        </Pane>
        <Pane>
          <Link to="/login">
            <Button
            >
              Login
            </Button>
          </Link>

          <Link to="/logout">
            <Button 
              onClick={() => {
                sessionStorage.clear();
              }}
            >
              Logout
            </Button>
          </Link>
        </Pane>
        
      </Pane>
      <Pane className={classes.contentContainer}>
        {props.children}
      </Pane>
    </Pane>
  )
}


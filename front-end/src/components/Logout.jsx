import { Heading } from "evergreen-ui";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "../hook/useToken";
import PageWrapper from "./PageWrapper";

export default function Logout(props) {
  const { token, setToken } = useToken();
  const navigate = useNavigate();

  useEffect(() => {
    if (!token) {
      navigate("/login")
    }
  })

  return (
    <PageWrapper>
      <Heading>Logout success</Heading>
    </PageWrapper>
  )
}
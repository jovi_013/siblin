import { Button, Heading, Pane, Text, TextInputField } from "evergreen-ui";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "../hook/useToken";
import PageWrapper from "./PageWrapper";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  lineBreak: {
    marginTop: "30px",
    marginBottom: "30px",
    borderColor: "#85A3FF",
  }
})

async function loginUser(credentials) {
  return fetch('http://localhost:8000/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then(data => data.json())
}

export default function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [walletAddress, setWalletAddress] = useState("");
  const classes = useStyles();
  const navigate = useNavigate();

  const { token, setToken } = useToken(); 

  useEffect(() => {
    if (token) {
      navigate("/")
    }
  })

  const handleSubmit = async e => {
    const _token = await loginUser({
      email,
      password
    });
    setToken(_token);
    navigate("/");
  }

  async function requestAccount() {
    console.log('Requesting account...');

    // ❌ Check if Meta Mask Extension exists 
    if(window.ethereum) {
      console.log('detected');

      try {
        const accounts = await window.ethereum.request({
          method: "eth_requestAccounts",
        });
        setWalletAddress(accounts[0]);
        setToken({"token": accounts[0]})
        navigate("/");
      } catch (error) {
        console.log('Error connecting...');
      }
    } else {
      alert('Metamask not detected');
    }
  }

  return (
    <PageWrapper>
      <Heading>Login (for issuer)</Heading>

      <TextInputField
        label="Email"
        description="Your email"
        placeholder="user@example.com"
        value={email}
        onChange={e => setEmail(e.target.value)}
        marginTop="20px"
        type="text"
      />

      <TextInputField
        label="Password"
        description="Your password"
        value={password}
        onChange={e => setPassword(e.target.value)}
        marginTop="20px"
        type="password"
      />

      <Button
        onClick={() => handleSubmit()}
        width="100%"
      >
        Submit
      </Button>

      <hr className={classes.lineBreak}/>

      <Pane display="flex" flexDirection="column" justifyContent="center" alignItems="center">
        <Heading>Shipper, Receiver, and Courier login here.</Heading>
        <Button
          marginY="10px"
          width="100%"
          onClick={async () => requestAccount()}
        >
          Connect wallet
        </Button>
      </Pane>

    </PageWrapper>
  )
}